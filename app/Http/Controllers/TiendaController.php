<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TiendaController extends Controller
{
    public function index(){
        return view('productos.index');
    }

    public function crear(){
        return view('productos.crear');
    }

    public function mostrar($producto = null){
        return view('productos.mostrar', compact('producto'));
    }
}
