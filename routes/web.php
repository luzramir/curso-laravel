<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\TiendaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', InicioController::class);
Route::get('productos', [TiendaController::class, 'index']);
Route::get('productos/crear', [TiendaController::class, 'crear']);
Route::get('productos/mostrar/{producto?}', [TiendaController::class, 'mostrar']);



